#!/bin/sh

. /opt/graphite/bin/activate

PATH="${PATH}:/usr/local/bin"

# remove stale pids
find /opt/graphite/storage -maxdepth 1 -name '*.pid' -delete